module.exports = {
  extends: 'airbnb-base',
  env: {
    node: true,
    mocha: true,
    jest: true,
  },
  plugins: ['mocha'],
  rules: {
    'no-console': 'off',
    'mocha/no-exclusive-tests': 'error',
    'no-underscore-dangle': 'off',
    'no-param-reassign': 'off',
    'arrow-parens': 'off',
    'function-paren-newline': 'off',
    'object-curly-newline': 'off',
  },
};
