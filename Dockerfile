FROM node:12

ARG MONGODB_URI

ENV ATLAS_HOST=${MONGODB_URI}

WORKDIR /usr/src/app/back

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3008

CMD ["npm", "run", "start:dev"]
