## NojeJS Backend Express

### Change:

- README.md

# Usage

## DB

MongoDB:

- DEV
  - **mongodb+srv://username:password@cluster0-myma9.mongodb.net/dev?retryWrites=true&w=majority**
- PRO
  - **mongodb+srv://username:password@cluster0-myma9.mongodb.net/pro?retryWrites=true&w=majority**

You can connect throw your favorite IDE or join atlas web interface here (will need login): https://cloud.mongodb.com/v2/5e483677cf09a237842b7a66#

## Routes

Folder: `api/` \
Filename: `index.js` \
Path sintax: https://expressjs.com/en/guide/routing.html \
Example:

```js
const services = require('../services');

const monit = require('./monit')(services);

module.exports = [...monit];
```

Points to:

```js
const DefaultController = require('./default.controller');

module.exports = ({ defaultService }) => {
  const defaultController = new DefaultController(defaultService);

  return [
    {
      method: 'get',
      path: '/mycustomroute', // See
      handler: defaultController.get,
      bindTo: defaultController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
  ];
};
```

## Controllers

Folder: `api/controllers/` \
Filename: `default.controller.js` \
Example:

```js
class DefaultController {
  constructor(defaultService) {
    this.defaultService = defaultService;
  }

  get(req, res) {
    res.send(this.defaultService.getStatus());
  }
}

module.exports = DefaultController;
```

## Services

Folder: `api/services/` \
Filename: `index.js` \
Example:

```js
const config = require('./../../config/server');

const defaultService = require('./default');

module.exports = {
  config,
  defaultService,
};
```

Folder: `api/services/health` \
Filename: `index.js` \
Example: **We create service instance here**

```js
const HealthService = require('./health.service');

const healthService = new HealthService();

module.exports = healthService;
```

Folder: `api/services/health` \
Filename: `health.service.js` \
Example:

```js
const { version } = require('../../../package.json');

class HealthService {
  constructor() {
    this.message = 'OK';
    this.version = version;
  }

  getStatus() {
    return { message: this.message, version: this.version };
  }
}

module.exports = HealthService;
```

## Middlewares

### PRE

```js
sentry;
cors;
helmet;
bodyParser;
logger;
jwt;
```

### POST

```js
errors;
```

## Auth

- jwt
- session

## Models

// TODO

## Errors

// TODO

### Important files and folders:

- Dockerfile:
- run.sh: required file to run the project in the server
- .gitlab-ci.yml: file to describe the build and deploy process in gitlab
- .eslintrc.js: file to define the eslint rules to apply
- README.md: file to describe the proyeto purpose and the important project information
- index.js: main app file. It initialize the express app and run the needed configuration(middlewares, routest,..)
- bin: contains js file to be executed from package.json
- config: contains config files
- middlewares: contains general middlewares for the project(express)
- api: contains the proyect functionality:

  - index.js: add routes in proyect
  - routes-boot: wrapper to initialize and configure all available routes
  - services: contains the services availables for being used in the controllers:

    - index.js: exports all available services
    - service-structure: every service must have its own folder. Inside the folder it should have an index which exports a service's instance. Each service is resposible to require its dependencies and build an instance

  - controllers: contains all controllers in the app. Each controller will have method that will mach wit available http routes:

    - index:js: it is responsible of export all routes config. It will require the available services and it can pass them to each controller to be used as dependencies.
    - controller-structure: each controller should be in its own folfer. Each folder should have a file (o several if the scope grows up an it is necesary to split the content) for the code and another for the tests. It also shold have and index.js, this file will be resonsible for creating the controller instance with the services pased as parameters and to export an array of available routes and and hanlder methods. Each element in this array should have:

      - method: http method
      - path: url path
      - handler: controller's method to handle the request
      - bindTo: controller instance
      - middlewares: array , list of method to use as middlewares
      - skipDefaultMiddlewares: boolean to skip or not de default middlewares(added in routes-boot.js)

#

# open ./coverage/lcov-report/index.html

# Comandos necesarios para el entorno

## Instalar

- `npm i`

## Arrancar

- `npm run start:dev`

# GIT

- origin: gitlab
  - connected to github by export methods

### Branches

Only **dev** and **master** branches are autodeployed
| repo | url |
| ------ | ------------------------------------------------------------------------------------ |
| gitlab | https://gitlab.com/onrubia78/gbp-api |
| github | https://github.com/Binomi0/gbp-api |
| heroku | https://git.heroku.com/gbp-api.git |
| own | http://ec2-3-133-211-246.us-east-2.compute.amazonaws.com/Binomio/gbp-api.git |

# DEPLOY

Autodeploys

- When merge or push into `dev`

Manual

- You push to gitlab and it is sinchronized with github. You can push a custom branch to gitlab and then go to github an open a MR to `dev` from your branch. This will create a unique deploy instance for your branch, and will show the URL in MR comments.

Location: HEROKU

### Process

// TODO

# SONAR

Add `"sonar": "sonar-scanner"` and run `npm run sonar`

```properties
sonar-project.properties
# must be unique in a given SonarQube instance
sonar.projectKey=gbpd-back
# --- optional properties ---

# defaults to project key
# sonar.projectName=Salvamento backend
# defaults to 'not provided'
# sonar.projectVersion=1.0

# Path is relative to the sonar-project.properties file. Defaults to .
sonar.sources=.

# local host
sonar.host.url=http://localhost:9000

# sonar login token
sonar.login=57c018a6e18a0ae8b784c642341817c9a21e1ef7

# sonar.coverage.exclusions=api/controllers/
sonar.coverage.exclusions=**/api/controllers/**,**/config/**,**/test/**,**/middlewares/**,**/projections/**,**/*.test.js,**/*.html,**/utils/**,**/modules/**,**/index.js,new-wc-release.js,**/api/services/push-notifications/**

# Encoding of the source code. Default is default system encoding
# sonar.sourceEncoding=UTF-8

# # sonar.coverageReportPaths=coverage/reports/test-reporter.xml
# sonar.testExecutionReportPaths=coverage/reports/test-reporter.xml
# sonar.javascript.lcov.reportPaths=coverage/lcov.info

```

sonar-scanner \
 -Dsonar.projectKey=gbpd-back \
 -Dsonar.sources=. \
 -Dsonar.host.url=http://localhost:9000 \
 -Dsonar.login=57c018a6e18a0ae8b784c642341817c9a21e1ef7
