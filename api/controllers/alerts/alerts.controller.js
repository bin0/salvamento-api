class AlertsController {
  constructor(alertsService, userService) {
    this.alertsService = alertsService;
    this.userService = userService;
  }

  getAll(req, res) {
    Promise.resolve()
      .then(() => this.alertsService.getAll())
      .then(res.send.bind(res));
  }

  getById(req, res) {
    Promise.resolve()
      .then(() => this.alertsService.getById(req.params._id))
      .then(res.send.bind(res));
  }

  getByUser(req, res) {
    Promise.resolve()
      .then(() => this.alertsService.getByUser(req.params.userId))
      .then(res.send.bind(res));
  }

  create(req, res) {
    Promise.resolve()
      .then(() => this.alertsService.create(req.params._id, req.body))
      .then(res.send.bind(res));
  }

  update(req, res) {
    Promise.resolve()
      .then(() => this.alertsService.update(req.params._id, req.body))
      .then(res.send.bind(res));
  }

  delete(req, res) {
    Promise.resolve()
      .then(() => this.alertsService.delete(req.params._id))
      .then(res.send.bind(res));
  }
}

module.exports = AlertsController;
