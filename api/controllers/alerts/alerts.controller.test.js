const { expect } = require('chai');
// const mongoose = require('mongoose');
// const request = require('supertest');
const AlertsController = require('./alerts.controller');
const AlertsService = require('../../services/alerts');
const UserService = require('../../services/user');
// const app = require('../../..');
// const requestLogin = require('../../../utils/request-login');
// const { UserModel } = require('../../../models');
// const db = require('../../../test/test-db');

// const defaultUser = {
//   email: 'test@email.com',
//   password: '123456',
//   isOnline: false,
// };

describe('--- ALERTS CONTROLLER ---', () => {
  // let auth;
  // before(async () => {
  //   await db(mongoose).connect();
  //   await UserModel.createUser(defaultUser);
  //   await requestLogin(app).then(res => {
  //     auth = res.token;
  //   });
  // });

  const alertsController = new AlertsController(AlertsService, UserService);

  it('should be defined', done => {
    expect(alertsController).to.be.an.instanceOf(AlertsController);
    done();
  });

  describe('/v1/alerts', () => {
    it('should be defined', done => {
      expect(alertsController.getAll).to.be.a('function');
      done();
    });
    // describe('when it runs', () => {
    //   it('should return status 200', done => {
    //     request(app)
    //       .get('/v1/alerts')
    //       .set('Authorization', `Bearer ${auth}`)
    //       .end((err, res) => {
    //         if (err) {
    //           console.log('Error', err);
    //           done();
    //         }
    //         if (res.status === 200) {
    //           console.log('ok1');
    //         }
    //         console.log('res', res.status);
    //         done();
    //       });
    //   });
    // });
  });
});
