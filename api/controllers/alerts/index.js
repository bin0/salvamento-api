const AlertsController = require('./alerts.controller');

const version = '/v1';

module.exports = ({ alertsService, userService }) => {
  const alertsController = new AlertsController(alertsService, userService);

  return [
    {
      method: 'get',
      path: `${version}/alerts`,
      handler: alertsController.getAll,
      bindTo: alertsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'get',
      path: `${version}/alerts/:_id`,
      handler: alertsController.getById,
      bindTo: alertsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'get',
      path: `${version}/alerts/user/:userId`,
      handler: alertsController.getByUser,
      bindTo: alertsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'post',
      path: `${version}/alerts/:_id`,
      handler: alertsController.create,
      bindTo: alertsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'put',
      path: `${version}/alerts/:_id`,
      handler: alertsController.update,
      bindTo: alertsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'delete',
      path: `${version}/alerts/:_id`,
      handler: alertsController.delete,
      bindTo: alertsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
  ];
};
