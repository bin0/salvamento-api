const services = require('../services');

const alerts = require('./alerts')(services);
const location = require('./location')(services);
const login = require('./login')(services);
const monit = require('./monit')(services);
const pushNotifications = require('./push-notifications')(services);
const settings = require('./settings')(services);
const teams = require('./teams')(services);
const user = require('./user')(services);

function get(req, res) {
  res.send('API REST Salvamento v1');
}

const home = {
  method: 'get',
  path: '/',
  handler: get,
  middlewares: [],
  skipDefaultMiddlewares: true,
};

module.exports = [
  home,
  ...alerts,
  ...location,
  ...login,
  ...monit,
  ...pushNotifications,
  ...settings,
  ...teams,
  ...user,
];
