const LocationController = require('./location.controller');

const version = '/v1';

module.exports = ({ locationService, userService }) => {
  const locationController = new LocationController(locationService, userService);

  return [
    {
      method: 'get',
      path: `${version}/locations`,
      handler: locationController.get,
      bindTo: locationController,
      middlewares: [],
    },
  ];
};
