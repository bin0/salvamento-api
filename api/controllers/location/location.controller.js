class LocationController {
  constructor(locationService, userService) {
    this.locationService = locationService;
    this.userService = userService;
  }

  get(req, res) {
    Promise.resolve()
      .then(() => this.locationService.get())
      .then(res.send.bind(res));
  }
}

module.exports = LocationController;
