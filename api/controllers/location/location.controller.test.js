const { expect } = require('chai');
const LocationController = require('./location.controller');

describe('--- LOCATION CONTROLLER ---', () => {
  it('should be defined', done => {
    expect(LocationController).to.be.a('function');
    done();
  });
});
