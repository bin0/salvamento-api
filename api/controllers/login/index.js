const LoginController = require('./login.controller');
const { loginErrors } = require('../../../errors');

const version = 'v1';

module.exports = ({ loginService, mailService }) => {
  const loginController = new LoginController(loginService, mailService, loginErrors);

  return [
    {
      method: 'post',
      path: `/${version}/login`,
      handler: loginController.logIn,
      bindTo: loginController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'get',
      path: `/${version}/logout/:uid`,
      handler: loginController.logOut,
      bindTo: loginController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'get',
      path: `/${version}/login/refresh/:uid`,
      handler: loginController.refreshToken,
      bindTo: loginController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'post',
      path: `/${version}/login/validate-token`,
      handler: loginController.validateToken,
      bindTo: loginController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'post',
      path: `/${version}/login/reset-password`,
      handler: loginController.recoveryPassword,
      bindTo: loginController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'post',
      path: `/${version}/login/unlock-code`,
      handler: loginController.unlockCode,
      bindTo: loginController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
  ];
};
