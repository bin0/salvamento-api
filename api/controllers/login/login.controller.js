/**
 * Login controller
 * handles HTTP responses
 */
class LoginController {
  constructor(loginService, mailService, loginErrors) {
    this.loginService = loginService;
    this.errors = loginErrors;
    this.mailService = mailService;
  }

  /**
   * Login a user
   */
  logIn(req, res) {
    console.log('logueando');
    if (req.session) {
      console.log('session.id:', req.session.id);
    }
    Promise.resolve()
      .then(() => this.loginService.logIn(req.body))
      .then(res.send.bind(res))
      .catch(this.errors.LoginError, err => res.status(401).send(err))
      .catch(err => res.status(401).send(err));
  }

  /**
   * Logout a user
   */
  logOut(req, res) {
    Promise.resolve()
      .then(() => this.loginService.logOut(req.params.uid))
      .then(() => res.sendStatus(204))
      .catch(this.errors.LoginError, err => res.status(err.code).send(err))
      .catch(err => res.status(500).send(err));
  }

  /**
   * Validate user token
   * @param {object} req Request object
   * @param {object} res Response object
   */
  validateToken(req, res) {
    Promise.resolve()
      .then(() => req.headers.authorization.split(' ')[1])
      .then(token => this.loginService.validateToken(token))
      .then(res.send.bind(res))
      .catch(this.errors.LoginError, err => res.status(err.code).send(err))
      .catch(err => res.status(err.code).send(err));
  }

  /**
   * Validate user token
   * @param {object} req Request object
   * @param {object} res Response object
   */
  refreshToken(req, res) {
    Promise.resolve()
      .then(() => this.loginService.refreshToken(req.params.uid))
      .then(res.send.bind(res))
      .catch(this.errors.LoginError, err => res.status(err.code).send(err))
      .catch(err => res.status(err.code).send(err));
  }

  resetPassword(req, res) {
    const url = `${req.headers.origin}/login`;
    Promise.resolve()
      .then(() => this.loginService.sendPasswordResetEmail(req.body.emailAddress, url))
      .then(res.sendStatus(202))
      .catch(this.errors.LoginError, err => res.status(err.code).send(err))
      .catch(err => res.status(err.code).send(err));
  }

  recoveryPassword(req, res) {
    Promise.resolve()
      .then(() => this.loginService.recoveryPassword(req.body))
      .then(code =>
        this.mailService.sendRecoveryPassword({
          to: req.body.emailAddress,
          subject: 'Código de confirmación de recuperación de contraseña',
          text: `Code: ${code}`,
        }),
      )
      .then(res.sendStatus(202));
    // .catch(this.errors.LoginError, err => res.status(err.code).send(err));
    // .catch(err => res.status(err.code).send(err));
  }
  unlockCode(req, res) {
    Promise.resolve()
      .then(() => this.loginService.unlockCode(req.body))
      .then(res.send.bind(res));
    // .catch(this.errors.LoginError, err => res.status(err.code).send(err));
    // .catch(err => res.status(err.code).send(err));
  }
}

module.exports = LoginController;
