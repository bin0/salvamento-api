const { expect } = require('chai');
const supertest = require('supertest');
const jsonwebtoken = require('jsonwebtoken');
const moment = require('moment');
const app = require('../../../');
const LoginController = require('./login.controller');

const version = 'v1';

describe('--- LOGIN CONTROLLER ---', () => {
  it('should be defined', done => {
    expect(LoginController).to.be.a('function');
    done();
  });

  describe('validateToken()', () => {
    const loginController = new LoginController();

    it('should be defined', done => {
      expect(loginController.validateToken).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      const token = {
        data: {
          _id: 'testuid',
        },
        aud: 'gbp',
        iat: Date.now(),
        exp: moment(Date.now())
          .add('15', 'minutes')
          .valueOf(),
      };
      const signedToken = jsonwebtoken.sign(token, process.env.JWT_SECRET);
      it('should return status 200 when passing the right token', done => {
        supertest(app)
          .post(`/${version}/login/validate-token`)
          .set('authorization', `Bearer ${signedToken}`)
          .end((req, res) => {
            expect(res).to.has.property('status', 200);
            done();
          });
      });
    });

    describe('when it has expired', () => {
      const token = {
        data: {
          _id: 'testuid',
        },
        aud: 'gbp',
        iat: Date.now(),
        exp: moment(Date.now())
          .subtract('15', 'minutes')
          .valueOf(),
      };
      const signedToken = jsonwebtoken.sign(token, process.env.JWT_SECRET);
      it('should return status 401 when passing outdated token', done => {
        supertest(app)
          .post(`/${version}/login/validate-token`)
          .set('authorization', `Bearer ${signedToken}`)
          .end((req, res) => {
            expect(res).to.has.property('status', 401);
            done();
          });
      });
    });
  });
});
