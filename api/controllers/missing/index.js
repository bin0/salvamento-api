const MissingController = require('./missing.controller');

const version = '/v1';

module.exports = ({ missingService }) => {
  const missingController = new MissingController(missingService);

  return [
    {
      method: 'get',
      path: `${version}/missing`,
      handler: missingController.get,
      bindTo: missingController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'post',
      path: `${version}/missing`,
      handler: missingController.create,
      bindTo: missingController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
  ];
};
