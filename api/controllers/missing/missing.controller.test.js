const { expect } = require('chai');
const MissingController = require('./missing.controller');

describe('--- MISSING CONTROLLER ---', () => {
  it('should be defined', done => {
    expect(MissingController).to.be.a('function');
    done();
  });
});
