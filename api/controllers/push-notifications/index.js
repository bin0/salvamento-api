const PushNotificationsController = require('./push-notifications.controller');

const version = '/v1';

module.exports = ({ pushNotificationsService, userService }) => {
  const pushNotificationsController = new PushNotificationsController(
    pushNotificationsService,
    userService,
  );

  return [
    {
      method: 'get',
      path: `${version}/push/subscription/:id`,
      handler: pushNotificationsController.sendPushNotification,
      bindTo: pushNotificationsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'post',
      path: `${version}/push/subscription`,
      handler: pushNotificationsController.handlePushNotificationSubscription,
      bindTo: pushNotificationsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
  ];
};
