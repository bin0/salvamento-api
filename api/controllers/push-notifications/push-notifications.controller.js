const Promise = require('bluebird');

class SettingsController {
  constructor(pushNotificationsService, userService) {
    this.pushNotificationsService = pushNotificationsService;
    this.userService = userService;
  }

  handlePushNotificationSubscription(req, res) {
    const { uid } = req.user.data;

    Promise.resolve()
      .then(() => this.pushNotificationsService.handlePushNotificationSubscription(req.body, uid))
      .then(subscriptionResponse => res.status(201).json(subscriptionResponse))
      .catch(error => {
        console.error('error', error);
        res.status(500).send(error);
      });
  }

  sendPushNotification(req, res) {
    const { id: subscriptionId } = req.params;

    Promise.resolve()
      .then(() => this.pushNotificationsService.sendPushNotification(subscriptionId))
      .then(() => res.status(202).json({}))
      .catch(error => {
        console.error('error', error);
        res.status(500).send(error);
      });
  }
}

module.exports = SettingsController;
