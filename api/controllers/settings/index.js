const SettingsController = require('./settings.controller');

const version = '/v1';

module.exports = ({ settingsService, userService }) => {
  const settingsController = new SettingsController(settingsService, userService);

  return [
    {
      method: 'put',
      path: `${version}/settings`,
      handler: settingsController.updateSettings,
      bindTo: settingsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
  ];
};
