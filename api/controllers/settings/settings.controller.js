class SettingsController {
  constructor(settingsService, userService) {
    this.settingsService = settingsService;
    this.userService = userService;
  }

  /* istanbul ignore next */
  updateSettings(req, res) {
    const { uid } = req.user.data;

    Promise.resolve()
      .then(() => this.userService.get(uid))
      .then(({ user }) => this.settingsService.updateSettings(user._id, req.body))
      .then(res.send.bind(res))
      .catch(error => {
        console.error('error', error);
        res.status(500).send(error);
      });
  }
}

module.exports = SettingsController;
