const { expect } = require('chai');
const SettingsController = require('./settings.controller');

describe('--- SETTINGS CONTROLLER ---', () => {
  it('should be defined', done => {
    expect(SettingsController).to.be.a('function');
    done();
  });
});
