const TeamsController = require('./teams.controller');

const version = '/v1';

module.exports = ({ teamsService }) => {
  const teamsController = new TeamsController(teamsService);

  return [
    {
      method: 'get',
      path: `${version}/teams`,
      handler: teamsController.getAll,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'get',
      path: `${version}/teams/:_id`,
      handler: teamsController.getById,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'get',
      path: `${version}/teams/user/:userId`,
      handler: teamsController.getByUser,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'post',
      path: `${version}/teams/:_id`,
      handler: teamsController.create,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'put',
      path: `${version}/teams/:_id`,
      handler: teamsController.update,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'delete',
      path: `${version}/teams/:_id`,
      handler: teamsController.delete,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'patch',
      path: `${version}/teams/join/:_id`,
      handler: teamsController.joinUserIntoTeam,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
    {
      method: 'patch',
      path: `${version}/teams/leave/:_id`,
      handler: teamsController.leaveUserIntoTeam,
      bindTo: teamsController,
      middlewares: [],
      skipDefaultMiddlewares: true,
    },
  ];
};
