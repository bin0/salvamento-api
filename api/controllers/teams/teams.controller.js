/* eslint-disable class-methods-use-this */

class TeamsController {
  constructor(teamsService) {
    this.teamsService = teamsService;
  }

  getAll(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.getAll())
      .then(res.send.bind(res));
  }

  getById(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.getById(req.params._id))
      .then(res.send.bind(res));
  }

  getByUser(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.getByUser(req.params.userId))
      .then(res.send.bind(res));
  }

  create(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.create(req.params._id, req.body))
      .then(res.send.bind(res));
  }

  update(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.update(req.params._id, req.body))
      .then(res.send.bind(res));
  }

  delete(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.delete(req.params._id))
      .then(res.send.bind(res));
  }

  joinUserIntoTeam(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.joinUserIntoTeam(req.params._id, req.body.userId))
      .then(res.send.bind(res));
  }

  leaveUserIntoTeam(req, res) {
    Promise.resolve()
      .then(() => this.teamsService.leaveUserIntoTeam(req.params._id, req.body.userId))
      .then(res.send.bind(res));
  }
}

module.exports = TeamsController;
