const multer = require('multer');

const upload = multer({
  dest: 'uploads/',
  limits: { fileSize: 1 * 1000 * 1000 },
});
const UserController = require('./user.controller');
const { userErrors } = require('../../../errors');

const version = '/v1';

module.exports = ({ userService, loginService, settingsService }) => {
  const userController = new UserController(userService, loginService, settingsService, userErrors);

  return [
    {
      method: 'get',
      path: `${version}/user`,
      handler: userController.get,
      bindTo: userController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'get',
      path: `${version}/user/:id`,
      handler: userController.getUserById,
      bindTo: userController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'post',
      path: `${version}/user/new`,
      handler: userController.create,
      bindTo: userController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'patch',
      path: `${version}/user/options`,
      handler: userController.updateField,
      bindTo: userController,
      middlewares: [],
      skipDefaultMiddlewares: false,
    },
    {
      method: 'patch',
      path: `${version}/user/avatar`,
      handler: userController.setUserAvatar,
      bindTo: userController,
      middlewares: [upload.single('avatar')],
      skipDefaultMiddlewares: false,
    },
  ];
};
