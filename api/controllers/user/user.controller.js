const Promise = require('bluebird');

class UserController {
  constructor(userService, loginService, settingsService, userErrors) {
    this.userService = userService;
    this.loginService = loginService;
    this.settingsService = settingsService;
    this.userErrors = userErrors;
  }

  /* istanbul ignore next */
  getUserById(req, res) {
    Promise.resolve()
      .then(() => this.userService.getUserById(req.params.id))
      .then(res.send.bind(res))
      .catch(this.userErrors.UserError, ({ code }) => {
        res.sendStatus(code);
      })
      .catch(error => {
        console.error('error', error);
        res.status(500).send(error);
      });
  }

  /* istanbul ignore next */
  get(req, res) {
    const { _id } = req.user.data;

    Promise.resolve()
      .then(() => this.userService.get(_id))
      .then(res.send.bind(res));
  }

  create(req, res) {
    Promise.resolve()
      .then(() => this.userService.create(req.body))
      .then(res.send.bind(res))
      .catch(error => {
        if (error.code === 11000) {
          console.log('El email ya existe');
          res.status(400).send(error.errmsg);
        } else {
          res.status(500).send(error);
        }
      });
  }

  updateField(req, res) {
    const { _id } = req.user.data;

    Promise.resolve()
      .then(() => this.userService.updateField(_id, req.body))
      .then(res.send.bind(res))
      .catch(error => {
        res.status(500).send(error);
      });
  }

  /* istanbul ignore next */
  setUserAvatar(req, res) {
    const { _id } = req.user.data;

    Promise.resolve()
      .then(() => this.userService.setUserAvatar(_id, req.file))
      .then(res.send.bind(res));
    // .catch(this.userErrors.UserError, ({ code, message }) => {
    //   res.status(code).send({ error: message, code });
    // })
    // .catch(({ code, message }) => {
    //   res.status(code).send({ code, error: message });
    // });
  }
}

module.exports = UserController;
