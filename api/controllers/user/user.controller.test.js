const { expect } = require('chai');
const UserController = require('./user.controller');

describe('--- USER CONTROLLER ---', () => {
  it('should be defined', done => {
    expect(UserController).to.be.a('function');
    done();
  });
});
