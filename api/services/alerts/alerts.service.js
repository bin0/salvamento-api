class AlertsService {
  constructor(AlertModel, UserModel) {
    this.alertModel = AlertModel;
    this.userModel = UserModel;
  }

  async getAll() {
    return Promise.resolve().then(() => this.alertModel.find());
  }

  async getById(_id) {
    return Promise.resolve().then(() => this.alertModel.findById(_id));
  }

  async getByUser(userId) {
    return Promise.resolve().then(() => this.alertModel.find({ user: userId }));
  }

  async create(_id, body) {
    return Promise.resolve()
      .then(() => this.alertModel.create({ ...body, user: _id, members: _id }))
      .then(async alert => {
        await this.userModel.findByIdAndUpdate(_id, { alerts: alert._id });
        return alert;
      });
  }

  async update(_id, body) {
    return Promise.resolve().then(() => this.alertModel.findByIdAndUpdate(_id, body));
  }

  async delete(_id) {
    return Promise.resolve().then(() => this.alertModel.findByIdAndRemove(_id));
  }
}

module.exports = AlertsService;
