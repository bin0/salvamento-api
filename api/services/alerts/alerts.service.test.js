const { expect } = require('chai');
const mongoose = require('mongoose');

const AlertsService = require('./alerts.service');
const { AlertModel, UserModel } = require('../../../models');
const db = require('../../../test/test-db');

describe('--- ALERTS SERVICE ---', () => {
  const alertId = new mongoose.Types.ObjectId();
  const missingId = new mongoose.Types.ObjectId();
  const userId = new mongoose.Types.ObjectId();
  const defaultAlert = {
    _id: alertId,
    user: userId,
    missing: missingId,
    name: 'testTeam',
    startDate: new Date(),
    plan: '',
  };

  before(async () => db(mongoose).connect());
  after(async () => db(mongoose).disconnect());
  const alertsService = new AlertsService(AlertModel, UserModel);

  it('AlertsService should an instanceof', done => {
    expect(alertsService).to.be.an.instanceOf(AlertsService);
    done();
  });

  describe('getAll()', () => {
    it('AlertsService.get should be a function', done => {
      expect(alertsService.getAll).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(() => AlertModel.create(defaultAlert));
      after(() => AlertModel.deleteMany({}));
      it('should return a alerts object', async done => {
        const alerts = await alertsService.getAll();
        expect(alerts).to.have.lengthOf(1);
        done();
      });
    });
  });

  describe('getById()', () => {
    it('AlertsService.getById should be a function', done => {
      expect(alertsService.getById).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(() => AlertModel.create(defaultAlert));
      after(() => AlertModel.deleteMany({}));
      it('should return a alerts object', async done => {
        const alerts = await alertsService.getById(alertId);
        // eslint-disable-next-line no-unused-expressions
        expect(alertId.equals(alerts._id)).to.be.true;
        done();
      });
    });
  });

  describe('getByUser()', () => {
    it('AlertsService.getByUser should be a function', done => {
      expect(alertsService.getByUser).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(() => AlertModel.create(defaultAlert));
      after(() => AlertModel.deleteMany({}));
      it('should return a alerts object', async done => {
        const [alerts] = await alertsService.getByUser(userId);
        // eslint-disable-next-line no-unused-expressions
        expect(userId.equals(alerts.user)).to.be.true;
        done();
      });
    });
  });

  describe('create()', () => {
    after(() => AlertModel.deleteMany({}));

    it('AlertsService.create should be a function', done => {
      expect(alertsService.create).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a alerts object', async done => {
        const alerts = await alertsService.create(userId, defaultAlert);

        expect(alerts).to.have.property('name', defaultAlert.name);
        done();
      });
    });
  });

  describe('update()', () => {
    it('AlertsService.update should be a function', done => {
      expect(alertsService.update).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(() => AlertModel.create(defaultAlert));
      after(() => AlertModel.deleteMany({}));
      it('should return a alerts object', async done => {
        await alertsService.update(alertId, { name: 'test name changed' });
        const alerts = await alertsService.getById(alertId);
        expect(alerts.name).to.equal('test name changed');
        done();
      });
    });
  });

  describe('delete()', () => {
    it('AlertsService.delete should be a function', done => {
      expect(alertsService.delete).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(() => AlertModel.create(defaultAlert));
      after(() => AlertModel.deleteMany({}));
      it('should return a alerts object', async done => {
        const alerts = await alertsService.delete(alertId);
        // eslint-disable-next-line no-unused-expressions
        expect(alertId.equals(alerts._id)).to.be.true;
        const alert = await alertsService.getById(alertId);
        // eslint-disable-next-line no-unused-expressions
        expect(alert).to.not.exist;
        done();
      });
    });
  });
});
