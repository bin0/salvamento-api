const AlertsService = require('./alerts.service');
const { AlertModel, UserModel } = require('../../../models');

const alertsService = new AlertsService(AlertModel, UserModel);

module.exports = alertsService;
