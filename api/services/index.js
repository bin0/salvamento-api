const config = require('./../../config/server');

const healtService = require('./health');
const locationService = require('./location');
const loginService = require('./login');
const mailService = require('./mail');
const missingService = require('./missing');
const pushNotificationsService = require('./push-notifications');
const settingsService = require('./settings');
const teamsService = require('./teams');
const userService = require('./user');

module.exports = {
  config,
  healtService,
  locationService,
  loginService,
  mailService,
  missingService,
  pushNotificationsService,
  settingsService,
  teamsService,
  userService,
};
