const LocationService = require('./location.service');
const { LocationModel, UserModel } = require('../../../models');
const { userErrors } = require('../../../errors');

const locationService = new LocationService(LocationModel, UserModel, userErrors);

module.exports = locationService;
