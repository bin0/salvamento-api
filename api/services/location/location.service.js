class LocationService {
  constructor(LocationModel, UserModel, userErrors) {
    this.LocationModel = LocationModel;
    this.UserModel = UserModel;
    this.userErrors = userErrors;
  }

  /**
   *
   * @param {ObjectId} locationId
   *
   * @return {object} location object
   */
  async get() {
    return this.LocationModel.find();
  }
}

module.exports = LocationService;
