const mongoose = require('mongoose');

const LocationService = require('./location.service');
const { LocationModel, UserModel } = require('../../../models');
const { userErrors } = require('../../../errors');
const db = require('../../../test/test-db');

describe('--- LOCATION SERVICE ---', () => {
  const userId = new mongoose.Types.ObjectId();
  const locationId = new mongoose.Types.ObjectId();
  const defaulLocation = {
    _id: locationId,
    user: userId,
    coords: {
      latitude: 39,
      longitude: 0.02,
    },
  };
  before(async () => db(mongoose).connect());
  after(async () => db(mongoose).disconnect());
  const locationService = new LocationService(LocationModel, UserModel, userErrors);

  it('LocationService should be defined', done => {
    expect(locationService).to.be.a('object');
    done();
  });

  describe('get()', () => {
    before(() => LocationModel.create(defaulLocation));
    after(() => LocationModel.deleteMany({}));
    it('LocationService.getUserLocation should be a function', done => {
      expect(locationService.get).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a list of locations', async done => {
        const result = await locationService.get();

        expect(result).to.have.lengthOf(1);
        done();
      });
    });
  });
});
