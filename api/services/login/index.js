const LoginService = require('./login.service');
const { UserModel } = require('../../../models');
const { loginErrors } = require('../../../errors');

const loginService = new LoginService(UserModel, loginErrors);

module.exports = loginService;
