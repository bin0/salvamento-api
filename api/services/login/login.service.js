const moment = require('moment');
const jwt = require('jsonwebtoken');
// const bcrypt = require('mongoose-bcrypt');

class LoginService {
  constructor(userModel, loginErrors) {
    this.errors = loginErrors;
    this.userModel = userModel;
  }

  /**
   *
   * @param {String} recipient Client Name
   */
  throwIfNotFound(user) {
    if (!user) {
      const { LoginError } = this.errors;
      throw new LoginError('Error al iniciar sesión', 500);
    }
    return user;
  }

  /**
   * Check for user and if found check for pasw to match
   *
   * @param {Object} userData Request body
   *
   * @function validateUser(object,object) Validate credentials
   *
   * @throws throwIfNotFound if no user is found
   *
   * @return {string|null} token
   */
  logIn({ email, password }) {
    return Promise.resolve()
      .then(() => this.userModel.findOne({ email }))
      .then(async user => {
        const isValid = await user.verifyPassword(password);
        return isValid && user;
      })
      .then(data => this.refreshToken(data))
      .then(this.throwIfNotFound.bind(this));
  }

  /**
   * Checks for a valid token
   *
   * @param {string} token
   *
   * @returns {token} A valid token with user credentials or null
   */
  validateToken(token) {
    if (!token) {
      throw new this.errors.LoginError('No se ha recibido token para validar', 400);
    }

    return Promise.resolve()
      .then(() => jwt.verify(token, process.env.JWT_SECRET))
      .catch(err => {
        throw new this.errors.LoginError(err);
      })
      .then(decoded => {
        const isExpired = moment(decoded.exp).isBefore(Date.now());
        if (isExpired) {
          throw new this.errors.LoginError('El token ha expirado', 401);
        }
        return decoded;
      })
      .then(({ data }) => this.refreshToken(data));
  }

  refreshToken(data) {
    if (!data || !data._id) {
      throw new this.errors.LoginError('No se ha recibido data', 400);
    }

    const token = {
      data: { _id: data._id },
      aud: 'gbp-token',
      iat: Date.now(),
      exp: moment(Date.now())
        .add('24', 'hours')
        .valueOf(),
    };

    return jwt.sign(token, process.env.JWT_SECRET);
  }

  async recoveryPassword({ emailAddress: email }) {
    const unlockCode = Math.floor(Math.random() * 10000);
    await this.userModel.findOneAndUpdate({ email }, { unlockCode });
    return unlockCode;
  }

  async unlockCode({ email, unlockCode }) {
    const user = await this.userModel.findOne({ email }, { unlockCode: 1 });
    if (user.unlockCode !== Number(unlockCode)) {
      return null;
    }
    await this.userModel.findOneAndUpdate({ email }, { unlockCode: null });
    return this.refreshToken({ _id: user._id });
  }
}

module.exports = LoginService;
