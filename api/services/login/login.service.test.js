const { expect } = require('chai');
const mongoose = require('mongoose');

const LoginService = require('./login.service');
const { UserModel } = require('../../../models');
const { loginErrors } = require('../../../errors');
const db = require('../../../test/test-db');

describe('--- ALERTS SERVICE ---', () => {
  const userId = new mongoose.Types.ObjectId();
  const defaultUser = {
    _id: userId,
    email: 'test@email.com',
    password: 'aR12003',
  };

  before(async () => db(mongoose).connect());
  after(async () => db(mongoose).disconnect());
  const loginService = new LoginService(UserModel, loginErrors);

  it('LoginService should an instanceof', done => {
    expect(loginService).to.be.an.instanceOf(LoginService);
    done();
  });

  describe('throwIfNotFound()', () => {
    it('LoginService.throwIfNotFound should be a function', done => {
      expect(loginService.throwIfNotFound).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      it('should return an error', async done => {
        Promise.resolve()
          .then(() => loginService.throwIfNotFound())
          .catch(() => done());
        done();
      });
      it('should return a user object response', async done => {
        const user = { email: 'test@email.com' };
        Promise.resolve()
          .then(() => loginService.throwIfNotFound(user))
          .then(() => {
            expect(user).to.have.property('email', 'test@email.com');
            done();
          });
        done();
      });
    });
  });

  describe('logIn()', () => {
    it('LoginService.logIn should be a function', done => {
      expect(loginService.logIn).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      before(() => UserModel.create(defaultUser));
      after(() => UserModel.deleteMany({}));
      it('should return a token', async done => {
        const login = await loginService.logIn(defaultUser);

        expect(login).to.be.a('string');
        done();
      });
    });
  });

  describe('validateToken()', () => {
    it('LoginService.validateToken should be a function', done => {
      expect(loginService.validateToken).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      before(() => UserModel.create(defaultUser));
      after(() => UserModel.deleteMany({}));
      it('should return a new token', async done => {
        const token = await loginService.logIn(defaultUser);
        const login = await loginService.validateToken(token);

        expect(login).to.be.a('string');
        done();
      });
    });

    describe('when it fails', () => {
      it('should return an error', async done => {
        Promise.resolve()
          .then(() => loginService.validateToken(null))
          .catch(() => done());
      });
      it('should return a LoginError', async done => {
        const token =
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
        Promise.resolve()
          .then(() => loginService.validateToken(token))
          .catch(() => done());
      });
    });
  });

  describe('refreshToken()', () => {
    it('LoginService.refreshToken should be a function', done => {
      expect(loginService.refreshToken).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      before(() => UserModel.create(defaultUser));
      after(() => UserModel.deleteMany({}));
      it('should return a new token', async done => {
        const token = await loginService.refreshToken({ _id: 'testId' });

        expect(token).to.be.a('string');
        done();
      });
    });

    describe('when it fails', () => {
      it('should return an error', async done => {
        Promise.resolve()
          .then(() => loginService.refreshToken())
          .catch(() => {
            done();
          });
      });
    });
  });

  describe('recoveryPassword()', () => {
    it('LoginService.recoveryPassword should be a function', done => {
      expect(loginService.recoveryPassword).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      before(() => UserModel.create(defaultUser));
      after(() => UserModel.deleteMany({}));
      it('should return a new token', async done => {
        const code = await loginService.recoveryPassword({ emailAddress: 'test@email.com' });

        expect(code).to.be.a('number');
        done();
      });
    });
  });

  describe('unlockCode()', () => {
    it('LoginService.unlockCode should be a function', done => {
      expect(loginService.unlockCode).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      before(() => UserModel.create(defaultUser));
      after(() => UserModel.deleteMany({}));
      it('should return a new token', async done => {
        const unlockCode = await loginService.recoveryPassword({ emailAddress: 'test@email.com' });
        const token = await loginService.unlockCode({ email: 'test@email.com', unlockCode });

        expect(token).to.be.a('string');
        done();
      });
    });

    describe('when it fails', () => {
      before(() => UserModel.create(defaultUser));
      after(() => UserModel.deleteMany({}));
      it('should return an error', async done => {
        const token = await loginService.unlockCode({ email: 'test@email.com', unlockCode: 4344 });

        // eslint-disable-next-line no-unused-expressions
        expect(token).to.be.null;
        done();
      });
    });
  });
});
