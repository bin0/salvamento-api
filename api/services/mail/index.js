const MailService = require('./mail.service');

const transporter = require('../../../modules/nodemailer');

const mailService = new MailService(transporter);

module.exports = mailService;
