class MailService {
  constructor(transporter) {
    this.transporter = transporter;
  }

  sendRecoveryPassword({ to, subject, text }) {
    return this.transporter.sendMail({
      from: 'gbpd@onrubia.es',
      to,
      subject,
      text,
      dsn: {
        id: 'some random message specific id',
        return: 'headers',
        notify: 'success',
        recipient: to,
      },
    });
  }
}

module.exports = MailService;
