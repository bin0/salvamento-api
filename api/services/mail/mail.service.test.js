const MailService = require('./mail.service');

describe('--- MAIL SERVICE ---', () => {
  const myMock = jest.fn();
  const transport = {
    // eslint-disable-next-line no-undef
    sendMail: myMock,
  };
  const mailService = new MailService(transport);

  it('MailService should an instanceof', done => {
    expect(mailService).to.be.an.instanceOf(MailService);
    done();
  });

  describe('sendRecoveryPassword()', () => {
    it('MailService.sendRecoveryPassword should be a function', done => {
      expect(mailService.sendRecoveryPassword).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should send an email', done => {
        const opts = {
          to: 'to@email.com',
          subject: 'This is a test email subject',
          text: 'This is a test email text',
        };
        mailService.sendRecoveryPassword(opts);
        done();
      });
    });
  });
});
