const MissingService = require('./missing.service');
const { MissingModel } = require('../../../models');

const missingService = new MissingService(MissingModel);

module.exports = missingService;
