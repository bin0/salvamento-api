class MissingService {
  constructor(MissingModel) {
    this.MissingModel = MissingModel;
  }

  /**
   *
   * @param {ObjectId} locationId
   *
   * @return {object} location object
   */
  async get() {
    return this.MissingModel.find();
  }
  /**
   *
   * @param {ObjectId} locationId
   *
   * @return {object} location object
   */
  async create(missing) {
    return this.MissingModel.create(missing);
  }
}

module.exports = MissingService;
