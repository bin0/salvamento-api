const { expect } = require('chai');
const mongoose = require('mongoose');

const MissingService = require('./missing.service');
const { MissingModel } = require('../../../models');
const db = require('../../../test/test-db');

describe('--- MISSING SERVICE ---', () => {
  const defaultMissing = {
    info: {
      name: 'Test',
      age: 79,
      sex: 'M',
    },
    lastSeenDate: new Date('2019-08-04'),
    lastLocation: 'Sagunto',
    briefing: '',
    status: '',
  };
  before(async () => db(mongoose).connect());
  after(async () => db(mongoose).disconnect());
  const missingService = new MissingService(MissingModel);

  it('MissingService should an instanceof', done => {
    expect(missingService).to.be.an.instanceOf(MissingService);
    done();
  });

  describe('get()', () => {
    it('MissingService.get should be a function', done => {
      expect(missingService.get).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(() => MissingModel.create(defaultMissing));
      after(() => MissingModel.deleteMany({}));
      it('should return a missing object', async done => {
        const missing = await missingService.get();
        expect(missing).to.have.lengthOf(1);
        done();
      });
    });
  });

  describe('create()', () => {
    it('MissingService.create should be a function', done => {
      expect(missingService.create).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a missing object', async done => {
        const missing = await missingService.create(defaultMissing);

        expect(missing.info).to.have.property('name', defaultMissing.info.name);
        expect(missing.info).to.have.property('age', defaultMissing.info.age);
        expect(missing.info).to.have.property('sex', defaultMissing.info.sex);
        expect(missing).to.have.property('lastLocation', defaultMissing.lastLocation);
        done();
      });
    });
  });
});
