const PushNotificationsService = require('./push-notifications.service');
const { UserModel } = require('../../../models');

const pushNotificationsService = new PushNotificationsService(UserModel);

module.exports = pushNotificationsService;
