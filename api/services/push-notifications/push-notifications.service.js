const crypto = require('crypto');
const webpush = require('web-push');

const vapidKeys = {
  privateKey: 'bdSiNzUhUP6piAxLH-tW88zfBlWWveIx0dAsDO66aVU',
  publicKey:
    'BIN2Jc5Vmkmy-S3AUrcMlpKxJpLeVRAfu9WBqUbJ70SJOCWGCGXKY-Xzyh7HDr6KbRDGYHjqZ06OcS3BjD7uAm8',
};

webpush.setVapidDetails('mailto:adolfo@onrubia.es', vapidKeys.publicKey, vapidKeys.privateKey);

function createHash(input) {
  const md5sum = crypto.createHash('md5');
  md5sum.update(Buffer.from(input));
  return md5sum.digest('hex');
}

class PushNotificationsService {
  constructor(userModel) {
    this.errorMessge = 'Ha ocurrido un error en [PushNotifications Service]';
    this.subscriptions = {};
    this.userModel = userModel;
    this.webpush = webpush;
  }

  async handlePushNotificationSubscription(subscriptionRequest, uid) {
    const subscriptionId = createHash(JSON.stringify(subscriptionRequest));

    const user = await this.userModel.findOneAndUpdate(
      { uid },
      { notificationSubscription: { [subscriptionId]: subscriptionRequest } },
      { upsert: true, new: true },
    );

    return { id: subscriptionId, user };
  }

  async sendPushNotification(subscription, notification = {}) {
    this.webpush
      .sendNotification(
        subscription,
        JSON.stringify({
          title: 'New Chat Available',
          text: 'HEY! Take a look at this chats!',
          image: '/images/jason-leung-HM6TMmevbZQ-unsplash.jpg',
          tag: 'new-chat',
          url: '/chats',
          ...notification,
        }),
      )
      .catch((err) => {
        console.log(err);
      });

    return {};
  }
}

module.exports = PushNotificationsService;
