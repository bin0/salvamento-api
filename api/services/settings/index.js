const SettingsService = require('./settings.service');
const { SettingsModel } = require('../../../models');

const userService = new SettingsService(SettingsModel);

module.exports = userService;
