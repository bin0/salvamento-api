class SettingsService {
  constructor(SettingsModel) {
    this.SettingsModel = SettingsModel;
    this.errorMessge = 'Ha ocurrido un error en [SettingsService]';
  }

  throwIfNotFound(params) {
    if (!params) {
      throw new Error(this.errorMessge);
    }
    return params;
  }

  async updateSettings(userId, settings) {
    return Promise.resolve()
      .then(() => this.throwIfNotFound(userId))
      .then(() => this.throwIfNotFound(settings))
      .then(() =>
        this.SettingsModel.findOneAndUpdate({ user: userId }, settings, {
          new: true,
        }),
      );
  }
}

module.exports = SettingsService;
