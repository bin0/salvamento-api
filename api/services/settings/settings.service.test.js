/* eslint-disable no-unused-expressions */

const mongoose = require('mongoose');
const SettingsService = require('./settings.service');
const { SettingsModel } = require('../../../models');
const db = require('../../../test/test-db');

describe('--- SETTINGS SERVICE ---', () => {
  const settingsId = new mongoose.Types.ObjectId();
  const userId = new mongoose.Types.ObjectId();
  const defaultSettings = {
    _id: settingsId,
    user: userId,
    visionRange: 10,
  };

  const settingsService = new SettingsService(SettingsModel);
  before(async () => {
    await db(mongoose).connect();
  });
  after(async () => db(mongoose).disconnect());

  it('SettingsService should be defined', done => {
    expect(settingsService).to.be.a('object');
    done();
  });

  describe('updateSettings()', () => {
    before(() => SettingsModel.create(defaultSettings));
    after(() => SettingsModel.deleteMany({}));
    it('userService.get should be defined', done => {
      expect(settingsService.updateSettings).to.be.a('function');
      done();
    });

    describe('when it runs', () => {
      it('should modify a setting', async done => {
        const result = await settingsService.updateSettings(userId, { visionRange: 20 });

        expect(result.visionRange).to.equal(20);
        done();
      });
    });

    describe('when it fails', () => {
      it('should throw an error if no user passed', async done =>
        settingsService.updateSettings(null, { visionRange: 20 }).catch(() => done()));
      it('should throw an error if no data received', async done =>
        settingsService.updateSettings(userId).catch(() => done()));
    });
  });
});
