const TeamsService = require('./teams.service');
const { TeamModel, UserModel } = require('../../../models');
const { ChatModel } = require('../../../models/Chat');

const teamsService = new TeamsService(TeamModel, UserModel, ChatModel);

module.exports = teamsService;
