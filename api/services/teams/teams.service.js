class TeamsService {
  constructor(teamModel, userModel, chatModel) {
    this.teamModel = teamModel;
    this.userModel = userModel;
    this.chatModel = chatModel;
  }

  async getAll() {
    return Promise.resolve().then(() => this.teamModel.find());
  }

  async getById(_id) {
    return Promise.resolve().then(() => this.teamModel.findById(_id).populate('members'));
  }

  async getByUser(userId) {
    return Promise.resolve().then(() => this.teamModel.find({ user: userId }));
  }

  async create(_id, body) {
    return Promise.resolve()
      .then(() => this.chatModel.create({ chatType: 'team' }))
      .then(chat => this.teamModel.create({ ...body, user: _id, members: _id, chat: chat._id }))
      .then(async team => {
        await this.userModel.findByIdAndUpdate(_id, { $push: { teams: team._id } });
        return team;
      });
  }

  async update(_id, body) {
    return Promise.resolve().then(() => this.teamModel.findByIdAndUpdate(_id, body));
  }

  async delete(_id) {
    return Promise.resolve()
      .then(() => this.teamModel.findByIdAndRemove(_id))
      .then(async team => {
        await this.chatModel.findByIdAndRemove(team.chat);
        return team;
      })
      .then(team => this.userModel.findByIdAndUpdate(team.user, { $pull: { teams: _id } }));
  }

  async joinUserIntoTeam(teamId, userId) {
    return Promise.resolve()
      .then(() => this.userModel.findByIdAndUpdate(userId, { $push: { teams: teamId } }))
      .then(() =>
        this.teamModel
          .findByIdAndUpdate(teamId, { $push: { members: userId } }, { new: true })
          .populate('members'),
      );
  }

  async leaveUserIntoTeam(teamId, userId) {
    return Promise.resolve().then(async () => {
      await this.userModel.findByIdAndUpdate(userId, { $pull: { teams: teamId } });
      const team = await this.teamModel
        .findByIdAndUpdate(teamId, { $pull: { members: userId } }, { new: true })
        .populate('members');
      return team;
    });
  }
}

module.exports = TeamsService;
