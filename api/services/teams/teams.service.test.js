const mongoose = require('mongoose');
const { TeamModel, UserModel } = require('../../../models');
const { ChatModel } = require('../../../models/Chat');

const TeamsService = require('./teams.service');
const db = require('../../../test/test-db');

describe('--- TEAMS SERVICE ---', () => {
  const teamId = new mongoose.Types.ObjectId();
  const userId = new mongoose.Types.ObjectId();
  const teamsService = new TeamsService(TeamModel, UserModel, ChatModel);
  before(async () => {
    await db(mongoose).connect();
  });
  after(async () => db(mongoose).disconnect());

  it('TeamsService should be defined', done => {
    expect(teamsService).to.be.a('object');
    done();
  });

  describe('getAll()', () => {
    after(() => TeamModel.deleteMany({}));
    it('TeamsService.get should be defined', done => {
      expect(teamsService.getAll).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return an empty array', async done => {
        const result = await teamsService.getAll();
        expect(result).to.have.lengthOf(0);
        done();
      });
      it('should return a list of teams', async done => {
        await TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]);

        const result = await teamsService.getAll();

        expect(result).to.have.lengthOf(1);
        done();
      });
    });
  });

  describe('getById()', () => {
    before(() => TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]));
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.get should be defined', done => {
      expect(teamsService.getById).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a team object', async done => {
        const result = await teamsService.getById(teamId);

        expect(result.name).to.equal('test team');
        done();
      });
    });
  });

  describe('getByUser()', () => {
    before(() => TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]));
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.getByUser should be defined', done => {
      expect(teamsService.getByUser).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a team object', async done => {
        const [result] = await teamsService.getByUser(userId);

        expect(result.name).to.equal('test team');
        done();
      });
    });
  });

  describe('create()', () => {
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.create should be defined', done => {
      expect(teamsService.create).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      before(async () => UserModel.create({ _id: userId, email: 't@t.es', password: 'aBc123' }));
      it('should return a new team object', async done => {
        const testTeam = {
          user: userId,
          name: 'test team',
        };
        const result = await teamsService.create(userId, testTeam);

        expect(result.name).to.equal('test team');
        done();
      });
    });
  });

  describe('update()', () => {
    before(() => TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]));
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.update should be defined', done => {
      expect(teamsService.update).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a new team object', async done => {
        const name = 'test team 2';
        await teamsService.update(teamId, { name });
        const result = await teamsService.getById(teamId);

        expect(result.name).to.equal('test team 2');
        done();
      });
    });
  });

  describe('delete()', () => {
    before(() => TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]));
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.delete should be defined', done => {
      expect(teamsService.delete).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a new team object', async done => {
        await teamsService.delete(teamId);
        const result = await teamsService.getById(teamId);

        // eslint-disable-next-line no-unused-expressions
        expect(result).to.not.exist;
        done();
      });
    });
  });

  describe('joinUserIntoTeam()', () => {
    before(() => TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]));
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.joinUserIntoTeam should be defined', done => {
      expect(teamsService.joinUserIntoTeam).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a team object', async done => {
        const result = await teamsService.joinUserIntoTeam(teamId, userId);

        expect(result.members).to.have.lengthOf(1);
        done();
      });
    });
  });

  describe('leaveUserIntoTeam()', () => {
    before(() => TeamModel.create([{ _id: teamId, user: userId, name: 'test team' }]));
    after(() => TeamModel.deleteMany({}));

    it('TeamsService.leaveUserIntoTeam should be defined', done => {
      expect(teamsService.leaveUserIntoTeam).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a team object', async done => {
        const result = await teamsService.leaveUserIntoTeam(teamId, userId);

        expect(result.members).to.have.lengthOf(0);
        done();
      });
    });
  });
});
