const UserService = require('./user.service');
const { UserModel, LocationModel, SettingsModel } = require('../../../models');
const { userErrors } = require('../../../errors');

const userService = new UserService(UserModel, LocationModel, SettingsModel, userErrors);

module.exports = userService;
