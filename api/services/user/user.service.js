const fs = require('fs');

class UserService {
  constructor(UserModel, LocationModel, SettingsModel, userErrors) {
    this.LocationModel = LocationModel;
    this.SettingsModel = SettingsModel;
    this.UserModel = UserModel;
    this.userErrors = userErrors;
  }

  throwIfNotFound(user) {
    if (!user) {
      const { UserError } = this.userErrors;
      throw new UserError();
    }
  }

  async get(_id) {
    return Promise.resolve().then(() => this.UserModel.findById(_id, { password: 0 }));
  }

  async getUserById(_id) {
    return Promise.resolve().then(() => this.UserModel.findById(_id));
  }

  async create(body) {
    return Promise.resolve()
      .then(() => this.throwIfNotFound(body))
      .then(() => this.UserModel.create(body));
  }

  async updateField(_id, fields) {
    return Promise.resolve().then(() => this.UserModel.findByIdAndUpdate(_id, { ...fields }));
  }

  /* istanbul ignore next */
  async setUserAvatar(_id, file) {
    if (!_id || !file) {
      // FIXME! ADOLFO throw custom error
      return null;
    }
    const newFile = fs.readFileSync(file.path);

    return this.UserModel.findByIdAndUpdate(_id, {
      avatar: { data: newFile, contentType: file.mimetype },
    });
  }
}

module.exports = UserService;
