const mongoose = require('mongoose');
const UserService = require('./user.service');
const { UserModel, LocationModel, SettingsModel } = require('../../../models');
const { userErrors } = require('../../../errors');
const db = require('../../../test/test-db');

describe('--- USER SERVICE ---', () => {
  const userId = new mongoose.Types.ObjectId();
  const userService = new UserService(UserModel, LocationModel, SettingsModel, userErrors);

  const defaultUser = {
    _id: userId,
    email: 'test@email.com',
    password: 'Abcde123',
  };

  before(async () => {
    await db(mongoose).connect();
  });
  after(async () => db(mongoose).disconnect());

  it('UserService should be defined', done => {
    expect(userService).to.be.a('object');
    done();
  });

  describe('get()', () => {
    before(() => UserModel.create(defaultUser));
    after(() => UserModel.deleteMany({}));
    it('userService.get should be defined', done => {
      expect(userService.get).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a user', async done => {
        const result = await userService.get(userId);

        // eslint-disable-next-line no-unused-expressions
        expect(userId.equals(result._id)).to.be.true;
        done();
      });
    });
  });

  describe('getUserById()', () => {
    before(() => UserModel.create(defaultUser));
    after(() => UserModel.deleteMany({}));
    it('userService.getUserById should be defined', done => {
      expect(userService.getUserById).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a user', async done => {
        const result = await userService.getUserById(userId);

        // eslint-disable-next-line no-unused-expressions
        expect(userId.equals(result._id)).to.be.true;
        done();
      });
    });
  });

  describe('create()', () => {
    after(() => UserModel.deleteMany({}));
    it('userService.create should be defined', done => {
      expect(userService.create).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      it('should return a user', async done => {
        const result = await userService.create(defaultUser);

        // eslint-disable-next-line no-unused-expressions
        expect(userId.equals(result._id)).to.be.true;
        done();
      });
    });

    describe('when it fails', () => {
      it('should return an empty object', async done => userService.create().catch(() => done()));
    });
  });

  describe('updateField()', () => {
    before(() => UserModel.create(defaultUser));
    after(() => UserModel.deleteMany({}));
    it('userService.updateField should be defined', done => {
      expect(userService.updateField).to.be.a('function');
      done();
    });
    describe('when it runs', () => {
      const newEmail = 'new-email@email.com';
      it('should update a user field', async done => {
        await userService.updateField(userId, { email: newEmail });
        const result = await UserModel.findById(userId);

        expect(result.email).to.equals(newEmail);
        done();
      });
    });
  });
});
