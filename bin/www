#!/usr/bin/env node

const app = require('../index');
const fs = require('fs');
const http = require('http');
const https = require('https');
const config = require('../config/server');
const io = require('../utils/socket/socket.io');

const port = process.env.PORT || config.port;
app.set('port', port);

let server;
if (process.env.NODE_ENV === 'production') {
  server = http.createServer(app);
} else {
  server = https.createServer(
    {
      key: fs.readFileSync('config/certs/gbp.key'),
      cert: fs.readFileSync('config/certs/gbp.cert'),
    },
    app,
  );
}

io(server);

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;

  console.log(`INITIALIZE: server on ${bind}`);
}

server.listen(port);
server.on('Server error', onError);
server.on('listening', onListening);
