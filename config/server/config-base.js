module.exports = {
  // any property that will be the same for all environments
  sessionConfig: {
    name: 'gbp-ses',
    secret: '$2a$10$Kya5BQDzAKtFlicJ5YnbB.FM8hibDxPr6vx/nwhehpA9LZUoe7mLe',
    timeout: 4.32e8, // 5 days,
    secure: false,
    resave: false,
    saveUninitialized: true,
  },
};
