module.exports = {
  port: 3008,
  jwtConfig: {
    secretKey: process.env.JWT_SECRET,
  },
  mongo: {
    uri: process.env.ATLAS_HOST,
    dataBaseName: 'salvamento',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    },
  },
};
