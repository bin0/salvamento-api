const loginErrors = require('./login');
const userErrors = require('./user');

module.exports = {
  loginErrors,
  userErrors,
};
