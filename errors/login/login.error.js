const { ITEM_NOT_FOUND_TXT, ITEM_NOT_FOUND_ERROR } = require('../../utils/constants');

/**
 * @param sdr Sender
 */
class LoginError extends Error {
  constructor(msg, code, status) {
    super(msg);
    this.status = status || 500;
    this.code = code || ITEM_NOT_FOUND_ERROR; // 401 || code || ITEM_NOT_FOUND_ERROR
    this.msg = msg || ITEM_NOT_FOUND_TXT;
    this.name = this.constructor.name;
  }
}

module.exports = {
  LoginError,
};
