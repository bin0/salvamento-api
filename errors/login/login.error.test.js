const { expect } = require('chai');
const LoginError = require('./login.error');

describe('--- LOGIN ERROR ---', () => {
  describe('when it runs', () => {
    it('should be defined', done => {
      expect(LoginError).to.be.an('object');
      done();
    });
  });

  describe('when have no properties', () => {
    const loginError = new LoginError.LoginError();
    it('should have default properties', done => {
      expect(loginError.code).to.equals(404);
      done();
    });
  });

  describe('when have defined properties', () => {
    const eventError = new LoginError.LoginError('test', 1003);
    it('should have defined properties', done => {
      expect(eventError.code).to.equals(1003);
      expect(eventError.msg).to.equals('test');
      done();
    });
  });
});
