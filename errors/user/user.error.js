const { ITEM_NOT_FOUND_TXT, ITEM_NOT_FOUND_ERROR } = require('../../utils/constants');

/**
 * @param sdr Sender
 */
class UserError extends Error {
  constructor(msg, code) {
    super(msg);
    this.code = code || ITEM_NOT_FOUND_ERROR; // 401 || code || ITEM_NOT_FOUND_ERROR
    this.msg = msg || ITEM_NOT_FOUND_TXT;
    this.name = this.constructor.name;
  }
}

module.exports = { UserError };
