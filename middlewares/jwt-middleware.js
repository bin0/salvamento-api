const jwt = require('express-jwt');
const moment = require('moment');

const { LoginError } = require('../errors/login/login.error');

function isRevoked(req, payload, done) {
  const isExpired = moment(payload.exp).isBefore(moment(Date.now()));
  if (isExpired) {
    const err = new LoginError('Autenticación expirada.', 1003, 401);
    done(err);
  } else {
    done(null, isExpired);
  }
}

function initJwt(app) {
  app.use(
    jwt({
      secret: process.env.JWT_SECRET,
      credentialsRequired: true,
      getToken: function fromHeaderOrQuerystring(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
          return req.headers.authorization.split(' ')[1];
        }
        return null;
      },
      isRevoked,
      requestProperty: 'user',
    }).unless({
      path: [
        '/monit/health',
        /^\/socket\/*/,
        '/v1/user/new',
        /^\/v1\/login\/*/,
        /^\/v1\/logout\/*/,
        '/',
        // /^\/v1\/push\/subscription\/*/,
      ],
      // custom: req => {
      //   const paths = [
      //     '/monit/health',
      //     '/v1/login',
      //     '/v1/login/validate-token',
      //     '/v1/user/new',
      //     /^\/v1\/push\/subscription\/*/,
      //   ];
      //   console.log('req.path', req.path);
      //   console.log(paths.includes(req.path));
      //   return req;
      // },
    }),
  );
  console.log('INITIALIZE: JWT middleware');
}

module.exports = initJwt;
