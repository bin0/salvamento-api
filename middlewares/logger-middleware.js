const chalk = require('chalk');
const moment = require('moment');
const { hasAnyKey } = require('../utils/validations');

function consoleLogger(req, res, next) {
  console.log(chalk.green(`[${moment().toString()}]`), chalk.green(`[${req.method}] =>`, req.url));
  if (hasAnyKey(req.query)) {
    console.log(chalk.yellow('req.query =>', JSON.stringify(req.query)));
  }
  if (hasAnyKey(req.params)) {
    console.log(chalk.blue('req.params =>', JSON.stringify(req.params)));
  }
  if (hasAnyKey(req.body)) {
    console.log(chalk.red('req.body =>', JSON.stringify(req.body)));
  }
  next();
}

function loggerMiddleware(app) {
  app.use(consoleLogger);
  console.log('INITIALIZE: logger middleware');
}

module.exports = loggerMiddleware;
