const corsMiddleware = require('./cors-middleware');
const helmetMiddleware = require('./helmet-middleware');
const bodyParserMiddleware = require('./body-parser-middleware');
const loggerMiddleware = require('./logger-middleware');
const jwtMiddleware = require('./jwt-middleware');
const sentryMiddleware = require('./sentry-middleware');
const dbMiddleware = require('./db-middleware');

module.exports = app => {
  sentryMiddleware(app);
  corsMiddleware(app);
  helmetMiddleware(app);
  bodyParserMiddleware(app);
  loggerMiddleware(app);
  jwtMiddleware(app);
  dbMiddleware(app);
};
