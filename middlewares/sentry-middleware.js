/* eslint-disable prefer-arrow-callback */

const Sentry = require('@sentry/node');
const { version } = require('../package.json');

function sentryMiddleware(app) {
  Sentry.init({
    dsn: process.env.SENTRY_DSN,
    environment: process.env.NODE_ENV,
    release: version,
  });

  app.use(Sentry.Handlers.requestHandler());

  // The error handler must be before any other error middleware and after all controllers
  app.use(Sentry.Handlers.errorHandler());

  // Optional fallthrough error handler
  app.use(function onError(err, req, res, next) {
    const sentry = `${res.sentry} \n`;
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    res.end(sentry);
    next();
  });
  console.log('INITIALIZE: sentry middleware');
}

module.exports = sentryMiddleware;
