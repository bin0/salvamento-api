const mongoose = require('mongoose');

const modelName = 'Alert';

const Alert = new mongoose.Schema(
  {
    missing: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'Missing',
      required: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'User',
    },
    name: { type: String, required: true },
    teams: [
      {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        ref: 'Team',
      },
    ],
    priority: { type: String, enum: ['high', 'med', 'low'], default: 'high' },
    startDate: { type: Date, required: true },
    plan: { type: String },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, Alert);
