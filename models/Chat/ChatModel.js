const mongoose = require('mongoose');

const modelName = 'Chat';

const ChatModel = new mongoose.Schema(
  {
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message', index: true }],
    accepted: { type: Boolean, default: false },
    status: { type: String, enum: ['open', 'closed'], default: 'open' },
    chatType: { type: String, enum: ['main', 'team'], default: 'main' },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, ChatModel);
