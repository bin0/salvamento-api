const mongoose = require('mongoose');

const modelName = 'Message';

const MessageModel = new mongoose.Schema(
  {
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat', required: true },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      index: true,
    },
    text: String,
    read: { type: Boolean, default: false },
    sent: { type: Boolean, default: false },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, MessageModel);
