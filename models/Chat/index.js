const ChatModel = require('./ChatModel');
const MessageModel = require('./MessageModel');

module.exports = { ChatModel, MessageModel };
