const mongoose = require('mongoose');

const modelName = 'Location';

const Location = new mongoose.Schema(
  {
    coords: {
      latitude: { type: Number, required: true },
      longitude: { type: Number, required: true },
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'User',
      required: true,
    },
    teams: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'Team',
    },
    range: { type: Number, default: 10 },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, Location);
