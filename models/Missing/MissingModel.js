const mongoose = require('mongoose');

const modelName = 'Missing';

const Missing = new mongoose.Schema(
  {
    info: {
      name: String,
      age: Number,
      sex: { type: String, enum: ['M', 'H'] },
    },
    lastSeenDate: Date,
    lastLocation: String,
    lastCoords: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'Location',
    },
    posibleLocations: [
      {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        ref: 'Location',
      },
    ],
    briefing: String,
    status: String,
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, Missing);
