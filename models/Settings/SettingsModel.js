const mongoose = require('mongoose');

const modelName = 'Settings';
const defaultDistance = 10;

const Settings = new mongoose.Schema(
  {
    visionRange: { type: Number, default: defaultDistance },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'User',
      required: true,
    },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, Settings);
