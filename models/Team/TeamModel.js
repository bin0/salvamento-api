const mongoose = require('mongoose');

const modelName = 'Team';

const Team = new mongoose.Schema(
  {
    alert: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'Alert',
    },
    locations: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location',
      },
    ],
    meetingPoint: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Location',
    },
    members: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    name: { type: String, required: true },
    status: { type: String, enum: ['open', 'closed'], default: 'open' },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'User',
      required: true,
    },
    chat: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Chat',
    },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

module.exports = mongoose.model(modelName, Team);
