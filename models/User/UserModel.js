const mongoose = require('mongoose');

const modelName = 'User';
const avatarTypes = ['image/jpg', 'image/jpeg', 'image/png'];

const User = new mongoose.Schema(
  {
    avatar: {
      data: Buffer,
      contentType: {
        type: String,
        enum: avatarTypes,
      },
    },
    alerts: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Alert',
      },
    ],
    displayName: { type: String, default: 'Invitado' },
    email: {
      type: String,
      index: true,
      required: true,
      unique: true,
    },
    found: { type: Number, default: 0 },
    isOnline: { type: Boolean, index: true },
    location: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
      ref: 'Location',
    },
    role: {
      type: String,
      enum: ['admin', 'manager', 'developer', 'user'],
      default: 'user',
      index: true,
    },
    password: { type: String, required: true, bcrypt: true },
    teams: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Team',
      },
    ],
    unlockCode: { type: Number, default: null },
  },
  {
    versionKey: false,
    timestamps: {},
  },
);

User.plugin(require('mongoose-bcrypt'));

module.exports = mongoose.model(modelName, User);
