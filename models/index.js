const mongoose = require('mongoose');

const AlertModel = require('./Alert');
const LocationModel = require('./Location');
const MissingModel = require('./Missing');
const SettingsModel = require('./Settings');
const TeamModel = require('./Team');
const UserModel = require('./User');

mongoose.Promise = Promise;

module.exports = {
  AlertModel,
  LocationModel,
  MissingModel,
  SettingsModel,
  TeamModel,
  UserModel,
  mongoose,
};
