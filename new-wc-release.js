/**
 * @author Adolfo Onrubia
 *
 * @name autoRelease
 * @description Creates a release branch on a project repo passing a
 * folder from here and a release version string
 *
 * @argument {string} release version to promote
 *
 * @example node new-wc-release.js 0.7.2
 * @returns {void}
 */

const chalk = require('chalk');
const readline = require('readline');
const fs = require('fs');
const { execSync } = require('child_process');

function createReleaseBranch() {
  const [, , release] = process.argv;

  if (!release) {
    console.error('Missing release version is required');
    process.exit(0);
  }

  execSync('git rev-parse --abbrev-ref HEAD > branch.txt');
  const readInterface = readline.createInterface({
    input: fs.createReadStream('branch.txt'),
    output: process.stdout,
    console: true,
  });
  readInterface.on('line', line => {
    if (line !== 'dev') {
      console.log(chalk.red('No puedes crear una versión desde la rama:'), chalk.blue(line));
      console.log(chalk.green('\nCambia a dev antes para lanzar una nueva release\n'));
      console.log(chalk.green('git checkout dev\n'));
      execSync('rm branch.txt');
      process.exit(0);
    }
  });

  fs.readFile('package.json', 'utf-8', (err, data) => {
    if (err) throw new Error(err);

    const _package = JSON.parse(data);

    if (release !== data.version) {
      execSync('git checkout dev');
      execSync('git pull -q');
      execSync(`git branch release/${release}`);
      execSync(`git checkout release/${release}`);

      _package.version = release;

      fs.writeFile('package.json', JSON.stringify(_package), 'utf-8', () => {
        execSync('npm run prettier:package');
        execSync('git add .');
        execSync(`git commit -m "update app version ${_package.version} => ${release}"`);
        execSync(`git tag -a v${release} -m "gbp-api ${release}"`);
        execSync(`git push -u -q origin release/${release}`);

        console.log('Version actualida a %s', release);
      });
    } else {
      console.log('Ya existe una release con la version %s', release);
    }
  });
}

createReleaseBranch();
