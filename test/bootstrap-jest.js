const chai = require('chai');
require('../config/polyfills');

// Uncomment if you use sinon-chai
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

global.context = describe;
global.before = beforeAll;
global.after = afterAll;
global.expect = chai.expect;

process.env.JWT_SECRET = '2a10Kya5BQDzAKtFlicJ5YnbBFM8hibDxPr6vxnwhehpA9LZUoe7mLe';

if (!process.env.LOG) {
  global.console.log = jest.fn();
  global.console.error = jest.fn();
}
