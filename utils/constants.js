const MAIL_NOT_FOUND_TXT = 'Correo electrónico no encontrado';

const CLIENT_NOT_FOUND_TXT = 'Cliente no encontrado';
const CLIENTS_NOT_FOUND_TXT = 'No se han encontrado clientes';
const CLIENTS_NOT_CREATED = 'No se ha podido crear el cliente';
const CLIENTS_NOT_DELETED = 'No se ha podido eliminar el cliente';
const CLIENT_ERROR = 'Ha ocurrido un error al recuperar el listado de clientes';
const EVENT_ERROR = 'Ha ocurrido un error sl lanzar el evento';
const MAIL_ERROR = 'Ha ocurrido un error al recuperar el listado de emails';
const LOGIN_ERROR = 4000;

const CRYPTOS = {
  'BTC-EUR': 'BTC-EUR',
  'BTC-USD': 'BTC-USD',
  'ETH-EUR': 'ETH-EUR',
};

const CLIENT_NOT_FOUND_ERROR = 404;
const MAIL_NOT_FOUND_ERROR = 404;
const ITEM_NOT_FOUND_ERROR = 404;
const ITEM_NOT_FOUND_TXT = 'No se ha encontrado el elemento.';

const ONCE_A_DAY = '0 8 * * *';
const EXPIRE_DAYS = 30;

module.exports = {
  CLIENT_NOT_FOUND_TXT,
  CLIENTS_NOT_FOUND_TXT,
  CLIENTS_NOT_CREATED,
  CLIENTS_NOT_DELETED,
  CLIENT_ERROR,
  CLIENT_NOT_FOUND_ERROR,
  MAIL_ERROR,
  MAIL_NOT_FOUND_TXT,
  MAIL_NOT_FOUND_ERROR,
  ITEM_NOT_FOUND_ERROR,
  ITEM_NOT_FOUND_TXT,
  LOGIN_ERROR,
  EVENT_ERROR,
  EXPIRE_DAYS,
  ONCE_A_DAY,
  CRYPTOS,
};
