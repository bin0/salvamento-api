/* eslint-disable import/no-extraneous-dependencies */
const supertest = require('supertest');

const defaultCredentials = {
  email: 'test@email.com',
  password: '123456',
};

const defaultApp = require('../../');

function request(app, session) {
  return (method, url) =>
    supertest(app)
      [method](url)
      .set('Authorization', `Bearer ${session.token}`)
      .set('Cookie', session.cookies);
}

function requestLogin(app = defaultApp, credentials = defaultCredentials) {
  return supertest(app)
    .post('/v1/login')
    .set('Accept', 'application/json')
    .send(credentials)
    .then(res => {
      const session = {
        token: res.text,
        cookies: res.headers['set-cookie'].pop().split(';')[0],
      };

      session.request = request(app, session);
      return session;
    })
    .catch(err => {
      console.error('Error =>', err);
    });
}

module.exports = requestLogin;
