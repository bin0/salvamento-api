const jwt = require('jsonwebtoken');

function isValid(token) {
  try {
    const { exp } = jwt.verify(token, process.env.JWT_SECRET);
    const now = Date.now();
    if (now > exp) {
      console.log('isExpired');
      return false;
    }
    return true;
  } catch (err) {
    return false;
  }
}

function authSocketMiddleware(socket, next) {
  const { token } = socket.handshake.query;
  if (isValid(token)) {
    return next();
  }
  return next(new Error('socket authentication error'));
}

module.exports = { authSocketMiddleware };
