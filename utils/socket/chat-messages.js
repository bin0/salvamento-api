const { ChatModel, MessageModel } = require('../../models/Chat');
const { UserModel } = require('../../models');

async function getTeamChatInfo(chatId) {
  try {
    return ChatModel.findById(chatId).populate('messages');
  } catch (err) {
    console.log('err', err);
    return null;
  }
}

async function deleteMessage(messageId) {
  try {
    return MessageModel.findByIdAndRemove(messageId);
  } catch (err) {
    console.log('err', err);
    return null;
  }
}

async function handleNewChatMessage(text, user, teamId) {
  try {
    const newMessage = await MessageModel.create({
      text,
      user,
      chat: teamId,
      sent: true,
    });
    if (!newMessage) {
      throw new Error('no se ha podido crear el mensaje');
    }
    await ChatModel.findByIdAndUpdate(
      teamId,
      {
        $push: { messages: newMessage._id },
      },
      { upsert: true },
    );

    return true;
  } catch (err) {
    console.error('ERROR =>', err);
    return false;
  }
}

async function handleMessageRead(socket, messageIds) {
  try {
    MessageModel.updateMany({ _id: { $in: messageIds } }, { read: true });
    socket.emit('updated-messages', null, true);
  } catch (err) {
    socket.emit('updated-messages', 'error', null);
    console.error(err);
  }
}

function handleUserConnected(email) {
  try {
    UserModel.findOneAndUpdate({ email }, { isOnline: true });
  } catch (err) {
    console.error(err);
  }
}

module.exports = {
  deleteMessage,
  getTeamChatInfo,
  handleMessageRead,
  handleNewChatMessage,
  handleUserConnected,
};
