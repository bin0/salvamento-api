const socketIo = require('socket.io');
const { UserModel } = require('../../models');
const PushNotificationService = require('../../api/services/push-notifications/push-notifications.service');
const {
  deleteMessage,
  getTeamChatInfo,
  handleNewChatMessage,
  handleMessageRead,
  // handleUserConnected,
} = require('./chat-messages');
const { authSocketMiddleware } = require('./auth');

module.exports = server => {
  const pushNotificationService = new PushNotificationService(UserModel);
  const sockets = new Map();
  const opts = {
    path: '/gbpd-sk',
    serveClient: false,
    // below are engine.IO options
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false,
  };
  const io = socketIo(server, opts);

  io.use(authSocketMiddleware);

  const teams = io.of('/teams');

  teams.on('connection', socket => {
    async function sendNewMessagesToClient(chatId) {
      const chatInfo = await getTeamChatInfo(chatId);
      if (chatInfo && chatInfo.messages) {
        teams.emit(`set-messages-${chatId}`, chatInfo.messages);
      }
    }
    socket.on('user-connected', async email => {
      if (!email) {
        throw new Error('Missing email field');
      }
      sockets.set(socket.id, email);
      teams.emit('connected-users-count', sockets.size);
    });

    socket.on('get-messages', chatId => {
      sendNewMessagesToClient(chatId);
    });

    socket.on('join-chat-room', chatId => {
      socket.join(chatId, err => {
        if (err) socket.emit('join-chat-error', err);
      });
    });

    socket.on('new-room-message', (roomId, event, message) => {
      io.to(roomId).emit(event, message);
    });

    socket.on('new-message', async (chatId, text, user) => {
      await handleNewChatMessage(text, user, chatId);
      sendNewMessagesToClient(chatId);
    });

    socket.on('delete-message', async messageId => {
      const message = await deleteMessage(messageId);
      if (message) {
        sendNewMessagesToClient(message.chat);
      }
    });

    socket.on('disconnect', () => {
      const email = sockets.get(socket.id);
      UserModel.findOneAndUpdate({ email }, { isOnline: false });
      sockets.delete(socket.id);
    });

    socket.on('message-read', messageIds => {
      handleMessageRead(socket, messageIds);
    });

    socket.on('error', error => {
      throw new Error(error);
    });
  });

  io.on('connection', socket => {
    socket.on('user-connected', async email => {
      if (!email) {
        throw new Error('Missing email field');
      }
      sockets.set(socket.id, email);
      io.sockets.emit('connected-users-count', sockets.size);
      // handleUserConnected(email);
    });

    socket.on('join-chat-room', chatId => {
      socket.join(chatId, err => {
        if (err) socket.emit('join-chat-error', err);
      });
    });

    socket.on('new-room-message', (roomId, event, message) => {
      io.to(roomId).emit(event, message);
    });

    socket.on('new-message', async (chatId, text, user) => {
      handleNewChatMessage(text, user, chatId, pushNotificationService);
      io.sockets.emit(chatId);
    });

    socket.on('disconnect', () => {
      const email = sockets.get(socket.id);
      UserModel.findOneAndUpdate({ email }, { isOnline: false });
      sockets.delete(socket.id);
    });

    socket.on('message-read', messageIds => {
      handleMessageRead(socket, messageIds);
    });

    socket.on('error', error => {
      throw new Error(error);
    });
  });
};
