const keys = require('lodash/keys');

function hasAnyKey(arg) {
  return !!keys(arg).length;
}

module.exports = {
  hasAnyKey,
};
